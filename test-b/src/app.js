import express from 'express'
import morgan from 'morgan'
import path from 'path'
import cors from 'cors'
import database from './database'

import { ApolloServer } from 'apollo-server-express'
import resolvers from './resolvers'
import typeDefs from './schemas'

// Settings
const app = express();
const env = process.env.NODE_ENV;

const gqlServer = new ApolloServer({
    typeDefs,
    resolvers
})

app.set('env', env);
app.set('ip', process.env.IP);
app.set('http-port', process.env.HTTP_PORT);
app.set('https-port', process.env.HTTPS_PORT);

// Middlewares
app.use(morgan(env === 'production' ? 'combined' : 'dev' ));
app.use(express.json());
app.use(cors());
gqlServer.applyMiddleware({app, path:`/${process.env.GQL_PATH}`});

// app.use(express.static(path.join(__dirname, process.env.PUBLIC_ROUTE)));

app.use('*', (req, res, next) => {
    res.status(404).send('Error 404 - Not found');
});

export default app