import mysql from 'mysql';

const database = {
    host: process.env.DB_HOST,
    database: process.env.DB_NAME,
    user: process.env.DB_USER,
    password: process.env.DB_PASS
};

const connection = mysql.createPool(database);

export default () => {
    return connection;
}