require('dotenv/config');
const mysql = require('mysql');

// Creación de tabla

const database = {
    host: process.env.DB_HOST,
    database: process.env.DB_NAME,
    user: process.env.DB_USER,
    password: process.env.DB_PASS
};

const cn = mysql.createConnection(database);

const CREATE_TABLE_QUERY = `
CREATE TABLE IF NOT EXISTS users_test_juanfernandogomezmaldonado (
    id INT PRIMARY KEY AUTO_INCREMENT,
    first_name VARCHAR(30) NOT NULL,
    middle_name VARCHAR(30),
    paternal_surname VARCHAR(30) NOT NULL,
    maternal_surname VARCHAR(30) NOT NULL,
    birthday DATE NOT NULL,
    email TEXT NOT NULL,
    phone_number VARCHAR(30) NOT NULL
);`;

cn.query(CREATE_TABLE_QUERY, (error, results, fields) => {

    if(error) {
        console.log(error);
    }

    console.log(results);
    process.exit();
    
});
