import dotenv from 'dotenv/config'
import database from './database'
import app from './app'
import http from 'http'
import https from 'https'

const HTTP_PORT = app.get('http-port')
const HTTPS_PORT = app.get('https-port')
const IP = app.get('ip')
const ENV = app.get('env')

const serverHttp = http.createServer()

const serverHttps = https.createServer(app.get('https-config'), app)

async function main() {
    try {

        if(ENV === 'production') {

            await serverHttp.listen(HTTP_PORT, IP, () => console.log('Server running on http'))

            await serverHttps.listen(HTTPS_PORT, IP, () => console.log('Server running on https'))

        } else {

            app.listen(HTTP_PORT, () => console.log(`Server running in development mode ${HTTP_PORT} : ${IP}`))

        }

    } catch (e) {

        console.log(`Error - 01 - ${e.message}`)

    }
}

main()