import { mergeTypeDefs } from 'graphql-tools'
import User from './user'

/* mergeTypeDefs en caso de tener que unir mas Schemas */
export default mergeTypeDefs([
    User
])