import { gql } from 'apollo-server-express'

const UserSchema = gql`

scalar Date

type User {
    id: Int
    first_name: String
    middle_name: String
    paternal_surname: String
    maternal_surname: String
    birthday: Date
    email: String
    phone_number: String
}

input UserInput {
    first_name: String!
    middle_name: String
    paternal_surname: String!
    maternal_surname: String!
    birthday: Date!
    email: String!
    phone_number: String!
}

type Query {
    getUsers: [User]
}

type Mutation {
    createUser(input: UserInput!): Int
}

`;

export default UserSchema