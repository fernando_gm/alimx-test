import { mergeResolvers } from 'graphql-tools'
import User from './user'

export default mergeResolvers([
    User
])