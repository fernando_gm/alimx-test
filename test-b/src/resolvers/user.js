import db from '../database'

export default {
    Query: {
        getUsers: async (_, { limit, offset }) => {
            try {
                const cn = db();
    
                let query = '';
    
                if (!limit && !offset) {
                    query = `SELECT * FROM users_test_juanfernandogomezmaldonado`;
                } else {
                    /* En caso de requerir paginación */
                    query = `SELECT * FROM users_test_juanfernandogomezmaldonado LIMIT ${limit ? limit : 0} OFFSET ${offset ? offset : 0}`;
                }
    
                cn.query(query, (error, results, fields) => {
                    if(error) {
                        console.log(error);
                    }
                    console.log(JSON.parse(JSON.stringify(results)))
                    return JSON.parse(JSON.stringify(results));
                });
                
            } catch (e) {
                console.log(e);

                return null
            }
        }
    },
    Mutation: {
        createUser: async (_, { input }) => {
            try {

                const {first_name, middle_name, paternal_surname, maternal_surname, birthday, email, phone_number } = input;
                
                const query = `INSERT INTO users_test_juanfernandogomezmaldonado (first_name, middle_name, paternal_surname, maternal_surname, birthday, email, phone_number) VALUES 
                ('${first_name}', '${middle_name}', '${paternal_surname}', '${maternal_surname}', '${birthday}', '${email}', '${phone_number}')
                `;

                const cn = db();

                await cn.query(query, (error, result, fields) => {
                    if(error) {
                        console.log(error);
                    }
                    console.log(result)


                    return 200
                });

            } catch (e) {
                console.log(e);

                return 500
            }
        }
    }
}