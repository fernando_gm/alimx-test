# API GraphQL - Alimx

## Inicializar la Base de Datos

Para asegurarse que existe la tabla con la que se trabajará, ejecute el comando `npm run dbinit`

## Iniciar proyecto

### Modo desarrollo

Ejecute el comando `npm run dev`

### Modo Producción

- Ejecute el comando `npm run build` para compilar el código

- Ejecute el comando `npm run start` para correr el servidor
