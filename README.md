# Instrucciones

## Prueba A - Formulario con entorno de chat

### Iniciar proyecto A

Ejecute el comando `npm start` ubicándose en `./alichat`

## Prueba B - API GraphQL

### Inicializar la Base de Datos

Para asegurarse que existe la tabla con la que se trabajará, ejecute el comando `npm run dbinit`

### Iniciar proyecto B

#### Modo desarrollo

Ejecute el comando `npm run dev`

#### Modo Producción

- Ejecute el comando `npm run build` para compilar el código

- Ejecute el comando `npm run start` para correr el servidor
