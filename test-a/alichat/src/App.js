import './App.css';
import { HashRouter, Switch, Route, Redirect} from 'react-router-dom'
import { ApolloClient, InMemoryCache, ApolloProvider} from '@apollo/client'

import Home from './components/views/Home';
import Create from './components/views/Create'
import Users from './components/views/Users';

const URI = process.env.REACT_APP_BACK_API_GRAPHQL;

const client = new ApolloClient({
  uri: URI,
  cache: new InMemoryCache({
    addTypename: false
  }),
  onError: (networkError, graphQLErrors) => {
    console.log('networkError:', networkError);
    console.log('graphqlError:', graphQLErrors);
  }
})

function App() {
  return (
    <ApolloProvider client={client}>
      <HashRouter>
        <Switch>
          <Route path='/home/' component={Home}/>
          <Route path='/create' component={Create}/>
          <Route path='/users/' component={Users}/>
          <Redirect from='/' to='/home/' exact />   
        </Switch>
      </HashRouter>
    </ApolloProvider>
  );
}

export default App;
