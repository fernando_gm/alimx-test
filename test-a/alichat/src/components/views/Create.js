import React, { useState } from 'react';
import { gql, useMutation } from '@apollo/client';
import Alert from '../shared/Alert';
import Layout from '../shared/Layout';
import QuestionForm from '../shared/QuestionForm';
import './css/create.css';

const CREATE_USER = gql`

mutation createUser($input: UserInput!) {
    createUser(input: $input)
}

`;

const Create = () => {

    /*  */

    const [createUser] = useMutation(CREATE_USER);

    /* Estado para los inputs */

    const [questions, setQuestions] = useState({
        nombre: '',
        segundo_nombre: '',
        apellido_paterno: '',
        apellido_materno: '',
        dia:'',
        mes:'',
        anio:'',
        correo:'',
        tel:''
    })


    /* Estado para las respuestas finales */

    const [textComplete, setTextComplete] = useState({
        fecha:{
            text:'Fecha de nacimiento',
            value: ''
        },
        correo: {
            text: 'Correo electrónico',
            value: ''
        },
        tel: {
            text: 'Teléfono celular',
            value: ''
        },
        nombre: {
            text: 'Nombre',
            value: ''
        }
    })


    /* Estado para determinar si esta completo el registro */

    const [isComplete, setComplete] = useState(false);


    /* Preguntas por sección */

    const inputsA = {
        nombre: 'Nombre',
        segundo_nombre: 'Segundo nombre',
        apellido_paterno: 'Apellido paterno',
        apellido_materno: 'Apellido materno'
    }

    const inputsB = {
        dia:'Día',
        mes:'Mes',
        anio:'Año'
    }

    const inputsC = {
        correo:'Correo electrónico',
        tel:'Teléfono celular'
    }


    /* Evento para actualizar el estado de los inputs */

    const handleInput = e => {
        setQuestions({
            ...questions,
            [e.target.name]:e.target.value
        })
    }


    /* Función para el evento 'onClick' del botón 'Iniciar' */

    const handleComplete = async() => {

        if (!checkInputs()) {
            alert(`Necesita introducir todos los datos`);
            return;
        }

        let nombre = `${questions.nombre.trim()} ${questions.segundo_nombre.trim()} ${questions.apellido_paterno.trim()} ${questions.apellido_materno.trim()}`;
        let fecha = `${questions.dia.trim()} ${questions.mes.trim()} ${questions.anio.trim()}`;

        const tel = formatPhone(questions.tel.trim());

        if(!tel) {
            alert('El número telefónico debe tener 10 dígitos');
            return;
        }

        let month = getMonth(questions.mes);

        if(month === null) {
            alert('El mes ingresado es invalido. Debe escribir correctamente el nombre del mes.')
            return;
        }

        setQuestions({
            ...questions,
            tel
        })

        setTextComplete({
            fecha:{
                text:'Fecha de nacimiento',
                value: fecha
            },
            correo: {
                text: 'Correo electrónico',
                value: questions.correo.trim()
            },
            tel: {
                text: 'Teléfono celular',
                value: tel
            },
            nombre: {
                text: 'Nombre',
                value: nombre
            }
        })

        fecha =  `${questions.anio.trim()}-${month}-${questions.dia.trim()}`;

        setComplete(true);

        console.log(fecha);

        const res = await createUser({
            variables: {
                input: {
                    first_name: questions.nombre.trim(),
                    middle_name: questions.segundo_nombre.trim(),
                    paternal_surname: questions.apellido_paterno.trim(),
                    maternal_surname: questions.apellido_materno.trim(),
                    birthday: fecha,
                    email: questions.correo.trim(),
                    phone_number: tel
                }
            }
        })

        console.log(res);
    }


    /* Función para revisar que todos los campos contengan datos */

    const checkInputs = () => {

        let isOkay = true;

        Object.keys(questions).map((key, idx) => {
            if(questions[key].trim() == '') {
                isOkay = false;
            }
            return true;
        })

        return isOkay;

    }

    /* Función para convertir el mes en un número */

    var months = {
        'enero' : '01',
        'febrero' : '02',
        'marzo' : '03',
        'abril' : '04',
        'mayo' : '05',
        'junio' : '06',
        'julio' : '07',
        'agosto' : '08',
        'septiembre' : '09',
        'octubre' : '10',
        'noviembre' : '11',
        'diciembre' : '12'
    }

    const getMonth = (month) => {

        month = month.trim().replace(' ', '').toLowerCase();

        if(!(month in months)) return null;

        return months[month];

    }

    /* Formatear el contenido del número de teléfono */
    const formatPhone = (tel) => {

        tel = tel.replace(/[^\d]/g, "");

        if (tel.length === 10) {

            return tel.replace(/(\d{3})(\d{3})(\d{4})/, "($1) $2-$3");
        }

        return null;
    }


    return (
        <Layout headTitle="AliChat" principalClass={'create-content-alimx'}>


            <div className="principal-title-c">
            
                <div className="content-c">
                
                    <div className="text-c">
                    
                        <h1>Datos personales</h1>
                    
                        <p> <i className="lni lni-timer"></i> En menos de 5 minutos</p>
                    
                    </div>

                    <div className="icon">
                    
                        <i className="lni lni-write"/>

                    </div>
                
                </div>

            
            </div>

            <div className="form-c">
            
                <QuestionForm title="¿Cúal es tu nombre?" inputs={inputsA} handleInput={handleInput}/>

                {isComplete ? <Alert uniqueValue={textComplete.nombre.value} color="pink"/> : ''}

                <QuestionForm title="¿Cúal es tu fecha de nacimiento?" inputs={inputsB} handleInput={handleInput}/>

                {isComplete ? <Alert uniqueValue={textComplete.fecha.value} color="pink"/> : ''}

                <QuestionForm title="Datos de contacto" inputs={inputsC} handleInput={handleInput}/>

                {isComplete ? <Alert content={textComplete} filter={inputsC} color="pink"/> : ''}

                <button onClick={handleComplete}>Iniciar</button>

                {isComplete ? <Alert content={textComplete} color="pink"/> : ''}
            
            </div>
            
        
        </Layout>
    );
};

export default Create;