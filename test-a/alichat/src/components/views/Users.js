import React from 'react';
import { gql, useQuery } from '@apollo/client'
import './css/users.css'
import Layout from '../shared/Layout';

const GET_USERS_QUERY = gql`

query getUsers {
    getUsers {
        first_name
        middle_name
        paternal_surname
        maternal_surname
        birthday
        email
        phone_number
    }
}

`;

const Users = () => {

    const { loading, error, data } = useQuery(GET_USERS_QUERY)

    if (loading) return ''
    if (error) return `Error ${error}`

    console.log('data>>',data)

    return (
        <Layout headTitle="AliChat" principalClass={'users-content-alimx'}>


        <div className="principal-title-c">
        
        <h1>Lista de Usuarios</h1>
        
        
        </div>

        <div className="content-c">

        

        <p><i className="lni lni-emoji-sad"></i></p>

        
        
        </div>
            
        
        </Layout>
    );
};

export default Users;