import React, { useState } from 'react';
import Alert from '../shared/Alert';
import Layout from '../shared/Layout';
import QuestionForm from '../shared/QuestionForm';
import './css/home.css'

const Home = () => {

    const answerA = `ReactJS es una herramienta muy poderosa que nos permite crear desde simples SPA hasta plataformas completas como Facebook.
    Al ser una librería y no un framework nos da una versatilidad para adaptar la estructura de un proyecto a conveniencia, además, aprender a
    utilizar ReactJS es muy fácil y rápido debido a lo intuitivo de su uso.`;

    const answerB = `Es una extensión de la sintaxis de javascript para el uso de ReactJS, nos permite combinar javascript y html.`;

    const answerC = `El estado son los datos o la información que tiene cada componente, es como almacenar datos en memoria.`;

    const answerD = `Se utiliza para actualizar el estado de un componente.`;

    const answerE = `Son propiedades que tienen los componentes. Estos pueden estar preestablecidos o pueden ser enviados como parámetros
    de un componente a otro.`;

    const answerF = `Así es, se puede utilizar librerías como "child_process."`;

    const answerG = `Una función que se manda como parámetro a otra función y esta es ejecutada en un determinado momento. Un ejemplo muy
    sencillo sería hacer una consulta a la base de datos y después ejecutar una función para hacer uso de los datos recibidos.`;

    const answerH = `Acciones que son ejecutadas al interactuar con elementos o componentes.`;

    const answerI = `Bloqueo : Se esperará hasta que termine una acción | No Bloqueo : No se espera a terminar una acción y continúa 
    ejecutando el código.`;

    const answerJ = `RESTful es un tipo de arquitectura y GraphQL es un lenguaje de consultas. Respecto a APIs; RESTful utiliza los 
    métodos HTTP (GET, POST, PUT, DELETE) dependiendo la acción a realizar. GraphQL solo utiliza el método POST. En un API REST se crean
    distintos ENDPOINTS para obtener datos, cada uno enlazado a un controlador, haciendo distintas llamadas a la API para obtener los 
    datos deseados. Con una API de GraphQL también debemos definir funciones para los datos que queremos devolver, mejor conocidas como 
    resolvers, pero si queremos obtener datos basta con hacer una única consulta.`;

    return (
        <Layout headTitle="AliChat" principalClass={'home-content-alimx'}>


        <div className="principal-title-c">
        
        <h1>Prueba Técnica - Alimx</h1>
        
        <h2>Por: Juan Fernando Gómez Maldonado</h2>
        
        </div>

        <div className="form-c">
        
        <h1>Cuestionario</h1>

        <QuestionForm title="¿Por qué deberíamos utilizar ReactJS?" text={answerA} />

        <QuestionForm title="¿Qué es JSX?" text={answerB} reverse={true} />

        <QuestionForm title='¿Qué es un "estado"?' text={answerC} />

        <QuestionForm title='¿Para qué se utiliza "setState"?' text={answerD}  />

        <QuestionForm title='¿Qué son los "props"?' text={answerE} />

        <QuestionForm title='¿NodeJS posee "subprocesos secundarios"?' text={answerF} reverse={true} />

        <QuestionForm title=' ¿Qué es un callback?' text={answerG} />

        <QuestionForm title='¿Qué es un "evento"?' text={answerH} reverse={true} />

        <QuestionForm title='¿Cuál es la diferencia entre funciones de "bloqueo" y "no bloqueo"?' text={answerI} reverse={true} />
        
        <QuestionForm title=' ¿Diferencia entre RESTful y GraphQL?' text={answerJ} />

        </div>
            
        
        </Layout>
    );
};

export default Home;