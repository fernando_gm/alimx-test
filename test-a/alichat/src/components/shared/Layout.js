import Navbar from './Navbar'
import { Fragment } from 'react'
import { Helmet } from 'react-helmet-async'
import './css/layout.css'

const Layout = ({children, headTitle, principalClass}) => {

    /*
    
    <String> headTitle: Titulo de la página actual

    <String> principalClass: Nombre de la clase unica para la vista

    */

    return ( 
        <Fragment>
            <Helmet>
                <title>{headTitle}</title>
                <meta name="description" content="Alimx test form" data-react-helmet="true"></meta>
                <link rel="icon" href="/favicon.ico" />
                <link rel="preconnect" href="https://fonts.gstatic.com"/>
                <link href="https://fonts.googleapis.com/css2?family=Nunito:wght@300;400;600;700&display=swap" rel="stylesheet"/>
                <link href="https://cdn.lineicons.com/2.0/LineIcons.css" rel="stylesheet"></link>
            </Helmet>

            <Navbar/>
            <div className={`principal-content-alimx ${principalClass}`}>
                {children}
            </div>


            <footer className="">
            </footer>
        </Fragment>
     );
}

export default Layout;