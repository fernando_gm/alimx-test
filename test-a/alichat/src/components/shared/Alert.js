import React from 'react';
import './css/alert.css'

const Alert = ( { content, filter, uniqueValue, color } ) => {

    /*
    
    <String> uniqueValue : Si el componente mostrará solo un texto simple, entonces este parametro debe ser pasado

    <Objeto> content: Se iterará cada item y mostrará los parámetros 'text' y 'value' del objeto 

    <Objeto> filter: Objeto que determina que items de 'content' mostrará

    */

    if (!uniqueValue && !filter) return (
        <div className={`alert-component-alimx ${color}`}>
            {Object.keys(content).map( (key, idx) => (
                <div key={key}><b>{content[key].text}</b>: {content[key].value}</div>
            ))}
        </div>
    );

    if (!uniqueValue) return (
        <div className={`alert-component-alimx ${color}`}>
            {Object.keys(filter).map( (key, idx) => (
                <div key={key}><b>{content[key].text}</b>: {content[key].value}</div>
            ))}
        </div>
    );

    return (
        <div className={`alert-component-alimx ${color}`}>
            {uniqueValue}
        </div>
    )
};

export default Alert;