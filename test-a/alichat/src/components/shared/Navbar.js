import React from 'react';
import { Link } from 'react-router-dom';
import './css/navbar.css'

const Navbar = () => {
    return (
        <nav>
            <div className="nav-content">
            
                <ul>
                
                    <li> <Link to="/home">Inicio</Link> </li>
                    <li> <Link to="/create">Crear</Link> </li>
                    <li> <Link to="/users">Usuarios</Link> </li>
                    <li> <a href="https://bitbucket.org/fernando_gm/alimx-test/src/master/">Repositorio</a> </li>

                </ul>

            </div>
        </nav>
    );
};

export default Navbar;