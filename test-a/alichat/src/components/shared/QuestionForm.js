import React from 'react';
import './css/questionForm.css'

const QuestionForm = ( { title, inputs, handleInput, text, reverse } ) => {

    /*
    
    <String> title: Titulo de la pregunta

    <Objeto> inputs: Objeto que determina la propiedad 'name' y 'placeholder' de cada input

    <Función> handleInput: Función a ejecutarse en el evento 'onChange' de cada input

    <String> text: Texto a mostrar como respuesta. Si este parámetro es recibido, entonces no se
                    mostrará el contenido de 'inputs'

    <Boolean> reverse: true -> Muestra la imagen del lado derecho

    */

    const showContent = () => {

        if(text) return (
            <div className="answer-cool">
            {text}
            </div>
        )

        return Object.keys(inputs).map((key, idx) => (
            <input className="input-c" name={key} key={key} placeholder={inputs[key]} onChange={handleInput} />
        ))
    }


    return (
        <div className={`question-form-alimx ${reverse ? 'reverse' : ''}`}>

            <div className="user-img-c">
            
                <img src="/img/default-img.jpg" alt=""/>
            
            </div>

            <div className="question-content-c">
            
                <div className="title-c">
                
                {title}
                
                </div>

                <div className="questions-c">
                
                {showContent()}

                </div>
            
            </div>
            
        </div>
    );
};

export default QuestionForm;